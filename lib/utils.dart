import 'package:geolocator/geolocator.dart';

class Utils {
  static double getSpeedKmh(Position position) {
    return position.speed * 3.6;
  }
}