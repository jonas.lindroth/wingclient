import 'package:flutter/material.dart';
import 'package:speed/utils.dart';
import 'package:rxdart/rxdart.dart';

import 'model/data.dart';
import 'speedometer/speedometer.dart';

class SpeedGauge extends SpeedOMeter {
  SpeedGauge({int start, int end, ThemeData themeData, PublishSubject<Data> dataObservable}) :
        super(start:start, end:end, highlightStart:0, highlightEnd:0, themeData: themeData,
          eventObservable: PublishSubject<List<double>>()) {
    dataObservable.where((event) => event?.position != null).map((event) => [Utils.getSpeedKmh(event.position), event.analyzed]).listen((event) => eventObservable.add(event));
  }

  factory SpeedGauge.withSubscriber({start, end, dataObservable}) {
    return new SpeedGauge(start:start, end:end, themeData:new ThemeData(
        primaryColor: Colors.blue,
        accentColor: Colors.black,
        backgroundColor: Colors.grey
    ), dataObservable:dataObservable);
  }

  void dispose() {
    eventObservable.close();
  }
}