import 'dart:async';
import 'dart:math';

import 'package:speed/analyzer/sliding_window.dart';
import 'package:speed/model/acceleration.dart';
import 'package:speed/model/sensor.dart';
import 'package:speed/settingsrepository.dart';

class Analyzer {
  StreamSubscription _dataSubscription;
  AnalyzerConfiguration configuration;
  SlidingWindow<Acceleration> _slidingWindow;
  double analyzed;
  Analyzer(this.configuration) {
    _slidingWindow = SlidingWindow(configuration.slidingDuration);
    SettingsRepository().settingsUpdated.listen((value) {
      _slidingWindow.size = Duration(milliseconds: value.slidingWindowSize.round());
    });
  }

  double analyze(Sensor sensor) {
    var result = _analyze(sensor.acceleration);
    _slidingWindow.add(sensor.acceleration, sensor.timestamp);
    return result;
  }

  double _analyze(Acceleration acceleration) {
    var window = _slidingWindow.window.toList();
    if (window.isEmpty)
      return 0;

    var minAcceleration = _getExtreme(window, min);
    var maxAcceleration = _getExtreme(window, max);

    var maxX = max((minAcceleration.x - acceleration.x).abs(), (maxAcceleration.x - acceleration.x).abs());
    var maxY = max((minAcceleration.y - acceleration.y).abs(), (maxAcceleration.y - acceleration.y).abs());
    var maxZ = max((minAcceleration.z - acceleration.z).abs(), (maxAcceleration.z - acceleration.z).abs());

    return sqrt(pow(maxX, 2) + pow(maxY, 2) + pow(maxZ, 2));
  }

  Acceleration _getExtreme(List<Acceleration> window, double compare(double a, double b)) {
    Acceleration ca;
    for (var wa in window) {
      ca = ca == null ? wa : Acceleration(
          compare(ca.x, wa.x), compare(ca.y, wa.y), compare(ca.z, wa.z));
    }
    return ca;
  }

  void dispose() {
    _dataSubscription.cancel();
  }
}

class AnalyzerConfiguration {
  Duration slidingDuration = Duration(milliseconds: SettingsRepository().slidingWindowSize.round());
}