import 'dart:collection';

class SlidingWindow<T> {
  final Queue<_WindowData<T>> _window = Queue<_WindowData<T>>();
  Duration size;
  SlidingWindow(this.size);

  add(T data, DateTime time) {
    _window.add(_WindowData<T>(data, time ?? DateTime.now()));
    _slide();
  }

  void _slide() {
    DateTime now = DateTime.now();
    _window.removeWhere((data) => data.time.isBefore(now.subtract(size)));
  }

  Iterable<T> get window => Queue<_WindowData<T>>.from(_window).map((e) => e.data);
}

class _WindowData<T> {
  final T data;
  final DateTime time;
  _WindowData(this.data, this.time);
}