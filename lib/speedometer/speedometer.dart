library speedometer;

import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'handpainter.dart';
import 'linepainter.dart';
import 'speedtextpainter.dart';
import 'package:rxdart/rxdart.dart';

class SpeedOMeter extends StatefulWidget {
  final int start;
  final int end;
  final double highlightStart;
  final double highlightEnd;
  final ThemeData themeData;

  final PublishSubject<List<double>> eventObservable;
  SpeedOMeter(
      {this.start,
      this.end,
      this.highlightStart,
      this.highlightEnd,
      this.themeData,
      this.eventObservable});

  @override
  _SpeedOMeterState createState() => new _SpeedOMeterState(this.start, this.end,
      this.highlightStart, this.highlightEnd, this.eventObservable);
}

class _SpeedOMeterState extends State<SpeedOMeter>
    with TickerProviderStateMixin {
  int start;
  int end;
  double highlightStart;
  double highlightEnd;
  PublishSubject<List<double>> eventObservable;

  List<double> val = [0.0, 0.0];
  List<double> newVal;
  double textVal = 0.0;
  AnimationController percentageAnimationController;
  StreamSubscription _subscription;

  _SpeedOMeterState(int start, int end, double highlightStart,
      double highlightEnd, PublishSubject<List<double>> eventObservable) {
    this.start = start;
    this.end = end;
    this.highlightStart = highlightStart;
    this.highlightEnd = highlightEnd;
    this.eventObservable = eventObservable;

    percentageAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1000))
      ..addListener(() {
        setState(() {
          val[0] = lerpDouble(val[0], newVal[0], percentageAnimationController.value);
          val[1] = lerpDouble(val[1], newVal[1], percentageAnimationController.value);
        });
      });
    _subscription = this.eventObservable.listen((values) {
      textVal = values.first;
      reloadData(values);
    }); //(value) => reloadData(value));
  }

  reloadData(List<double> values) {
    newVal = values;
    percentageAnimationController.forward(from: 0.0);
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).isCurrent == false) {
      return Text("");
    }
    return new Center(
      child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return new Container(
          height: constraints.maxWidth,
          width: constraints.maxWidth,
          child: new Stack(fit: StackFit.expand, children: <Widget>[
            new Container(
              child: new CustomPaint(
                  foregroundPainter: new LinePainter(
                      lineColor: this.widget.themeData.backgroundColor,
                      completeColor: this.widget.themeData.primaryColor,
                      startValue: this.start,
                      endValue: this.end,
                      startPercent: this.widget.highlightStart,
                      endPercent: this.widget.highlightEnd,
                      width: 40.0)),
            ),
            new Center(
                //   aspectRatio: 1.0,
                child: new Container(
                    height: constraints.maxWidth,
                    width: double.infinity,
                    padding: const EdgeInsets.all(20.0),
                    child: new Stack(fit: StackFit.expand, children: <Widget>[
                      new CustomPaint(
                        painter: new HandPainter(
                            value: val[0],
                            start: this.start,
                            end: this.end,
                            color: Colors.orange),
                      ),
                      new CustomPaint(
                        painter: new HandPainter(
                            value: val[1],
                            start: this.start,
                            end: this.end,
                            color: this.widget.themeData.accentColor),
                      ),
                    ]))),
            new Center(
              child: new Container(
                width: 30.0,
                height: 30.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  color: this.widget.themeData.backgroundColor,
                ),
              ),
            ),
            new CustomPaint(
                painter: new SpeedTextPainter(
                    start: this.start, end: this.end, value: this.textVal)),
          ]),
        );
      }),
    );
  }

  @override
  void dispose() {
    _subscription.cancel();
    deactivate();
    percentageAnimationController.dispose();
    super.dispose();
  }
}
