import 'dart:typed_data';

class Parser {
  static String _data;
  static void onData(Uint8List buffer, int backspacesCounter, void setMessage(String message)) {
    var data = String.fromCharCodes(buffer);
    if (_data == null) {
      var start = buffer.indexOf(88);
      if (start >= 0) {
        _data = data.substring(start);
      } else {
        return null;
      }
    }
    var stop = buffer.indexOf(67);
    if (stop >= 0) {
      var message = _data + data.substring(0, stop+1);
      _data = data.substring(stop+1);
      setMessage(_convertMessage(message));
    } else {
      _data += data;
    }
  }
  static String _convertMessage(String message) {
    var converted = message.replaceAll("\r\n", "").replaceAll(" ", "");
    converted = converted.replaceAll("X:", "");
    converted = converted.replaceAll("Y:", ";");
    converted = converted.replaceAll("Z:", ";");
    converted = converted.replaceAll("m/s^2", ";");
    converted = converted.replaceAll("meters", ";");
    converted = converted.replaceAll("*C", "");
    return converted;
  }
}