import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class LinearChart extends StatefulWidget {
  final PublishSubject<LinearData> dataObservable = new PublishSubject();
  final bool animate;
  final List<LinearData> _data = List<LinearData>();

  LinearChart({this.animate, List<LinearData> data}) {
    for (var item in data) {
      _data.add(item);
    }
  }

  factory LinearChart.create(List<LinearData> data) {
    return new LinearChart(
      animate: false,
      data: data
    );
  }

  void dispose()
  {
    dataObservable.close();
  }

  @override
  _LinearChartState createState() => _LinearChartState(dataObservable);
}

class _LinearChartState extends State<LinearChart> {
  _LinearChartState(dataObservable) {
    dataObservable.listen(_onData);
  }

  void _onData(LinearData data) {
    setState(() {
      if (data != null)
        widget._data.add(data);
      else
        widget._data.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      _createSampleData(),
      animate: widget.animate,
      dateTimeFactory: const charts.LocalDateTimeFactory(),
    );
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<LinearData, DateTime>> _createSampleData() {
    return [
      new charts.Series<LinearData, DateTime>(
        id: 'x',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearData s, _) => s?.timestamp,
        measureFn: (LinearData s, _) => s?.data["x"],
        data: widget._data,
      ),
      new charts.Series<LinearData, DateTime>(
        id: 'y',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (LinearData s, _) => s?.timestamp,
        measureFn: (LinearData s, _) => s?.data["y"],
        data: widget._data,
      ),
      new charts.Series<LinearData, DateTime>(
        id: 'z',
        colorFn: (_, __) => charts.MaterialPalette.yellow.shadeDefault,
        domainFn: (LinearData s, _) => s?.timestamp,
        measureFn: (LinearData s, _) => s?.data["z"],
        data: widget._data,
      )
      ,
      new charts.Series<LinearData, DateTime>(
        id: 'crash',
        colorFn: (_, __) => charts.MaterialPalette.black,
        domainFn: (LinearData s, _) => s?.timestamp,
        measureFn: (LinearData s, _) => s?.data["a"],
        data: widget._data,
      )
    ];
  }
}

class LinearData {
  final String id;
  final DateTime timestamp;
  final Map<String, double> data;

  LinearData(this.id, this.timestamp, this.data);
}