import 'dart:async';
import 'dart:typed_data';
import 'package:speed/model/sensor.dart';
import 'package:speed/parser.dart';
import 'package:rxdart/rxdart.dart';
import 'devices/bluetooth_device.dart';
import 'model/acceleration.dart';
import 'devices/connection.dart';

class Accelerator {

  Accelerator._privateConstructor();

  static final Accelerator _instance = Accelerator._privateConstructor();

  factory Accelerator() {
    return _instance;
  }

  final PublishSubject<Sensor> accelerationObserver = new PublishSubject();
  final PublishSubject<Device> statusObserver = new PublishSubject();

  bool isConnected = false;
  StreamSubscription _deviceSubscriber;
  Connection connection;

  String get deviceName => connection?.device?.device?.name ?? "-";

  void start(String name) {
    Connection.createWithName(name).then((value) {
      connection = value;
      connection.connectionStatus.listen(_onConnectionStatus);
      connection.dataStream.listen(_onDataReceived);
    });
  }

  void _onConnectionStatus(bool connected) {
    isConnected = connected;
    statusObserver.add(connection.device);
  }

  void _onDataReceived(Uint8List data) {
    var backspacesCounter = data.where((b) => b == 8 || b == 127).length;
    if (backspacesCounter > 0) print("BackspaceCounter: $backspacesCounter");

    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    // Apply backspace control character
    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      }
      else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        }
        else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }
    Parser.onData(buffer, backspacesCounter, (message) => accelerationObserver.add(_mapMessage(message)));
  }

  Sensor _mapMessage(String message) {
    var items = message.split(";").map((e) => double.tryParse(e) ?? "0").toList();
    return items.length == 5 ? Sensor(acceleration: Acceleration(items[0], items[1], items[2]), height:items[3], temperature:items[4]) : null;

  }

  void dispose() {
    if (_deviceSubscriber != null)
      _deviceSubscriber.cancel();
    accelerationObserver.close();
  }
}
