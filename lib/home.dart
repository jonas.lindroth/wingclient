

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:speed/accelerator.dart';
import 'package:speed/dashboard.dart';
import 'package:speed/devices/bluetooth_device.dart';
import 'package:speed/devices/select_device_page.dart';
import 'package:speed/settings.dart';
import 'package:speed/start.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState()  => _Home();

}

class _Home extends State<Home> {
  Widget mainWidget = Start();
  bool _bluetoothConnected = false;
  StreamSubscription _subscriber;
  @override
  void initState() {
    super.initState();
    _subscriber = Accelerator().statusObserver.listen((value) {
      setState(() {
        _bluetoothConnected = value.availability == DeviceAvailability.yes;
      });
    });
  }

  void dispose() {
    super.dispose();
    _subscriber.cancel();
  }

  @override
  Widget build(BuildContext context) {
    var blueIcon = Icon(
      _bluetoothConnected ? Icons.bluetooth_connected : Icons.bluetooth_disabled,
      color: _bluetoothConnected ? Colors.green : Colors.red,
    );
    // TODO: implement build
    return new Scaffold(
      appBar: AppBar(
        title: Text(""),
        actions: <Widget>[
          IconButton(
            icon: blueIcon,
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: new Column(
                children: <Widget>[
                  Text('Version: 0.0.1', textAlign: TextAlign.left),
                  Padding(
                      child: Image.asset('assets/images/logo.png', width: 200),
                      padding: new EdgeInsets.all(0.0))
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
              decoration: BoxDecoration(
              color: Colors.blue,
              )
            ),
            ListTile(
              title: Text('Settings'),
              onTap: () {
                setState(() {
                  mainWidget = Settings();
                });
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('Dashboard'),
              onTap: () {
                setState(() {
                  mainWidget = Dashboard();
                });
                Navigator.pop(context);
                // Update the state of the app
                // ...
                // Then close the drawer
                //Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: mainWidget,
    );
  }
}