import 'dart:async';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:speed/location.dart';
import 'package:rxdart/rxdart.dart';
import 'package:speed/settingsrepository.dart';
import 'package:speed/utils.dart';
import 'accelerator.dart';
import 'analyzer/analyzer.dart';
import 'model/data.dart';

class DataCollector {
  DataCollector._privateConstructor();

  static final DataCollector _instance = DataCollector._privateConstructor();

  factory DataCollector() {
    _instance.start();
    return _instance;
  }
  bool _started = false;
  bool _manualCrash = false;
  bool _active = false;

  Accelerator _accelerator;
  Location _location;
  Position _currentPosition;
  StreamSubscription _positionSubscription;
  StreamSubscription _accelerationSubscription;
  final PublishSubject<Data> dataObserver = PublishSubject();
  final List<Data> messages = List<Data>();
  Analyzer _analyzer;

  void start() {
    if (_started) return;
    _started = true;
    _accelerator = Accelerator();
    _accelerator.start("Wing");
    _location = Location();
    _analyzer = Analyzer(AnalyzerConfiguration());

      if (!SettingsRepository().active) {
        _positionSubscription = _location.positionObservable.listen((value) {
        _currentPosition = value;

          var message = Data(_currentPosition, null, 0, _manualCrash);
          _manualCrash = false;
          dataObserver.add(message);
          messages.add(message);
        });
      } else {
        _accelerationSubscription = _accelerator.accelerationObserver.listen((value) {
          _location.getPosition().then((value) => _currentPosition = value);
          var message = Data(_currentPosition, value, _analyzer.analyze(value), _manualCrash);
          _manualCrash = false;
          dataObserver.add(message);
          messages.add(message);
        });
      }


  }

  Future uploadData(user, data) {
    return FirebaseStorage.instance.ref()
        .child("speed/$user ${DateTime.now().toString()}.csv").putString(data);
  }

  String getMessages() {
    var data = new StringBuffer();
    data.writeln("Time;X;Y;Z;Height;Temp;Speed;Latitude;Longitude;Acceleration;Crash");
    for (var m in messages) {
      data.writeln("${m.sensor?.timestamp != null ? m.sensor.timestamp.toString() : m.position?.timestamp?.toString()};"
          "${m.sensor?.acceleration?.x ?? 0};"
          "${m.sensor?.acceleration?.y ?? 0};"
          "${m.sensor?.acceleration?.z ?? 0};"
          "${m.sensor?.height ?? 0};"
          "${m.sensor?.temperature ?? 0};"
          "${m.position != null ? Utils.getSpeedKmh(m.position) : 0};"
          "${m.position?.latitude?.toString() ?? ""};"
          "${m.position?.longitude?.toString() ?? ""};"
          "${m.analyzed.toString()};"
          "${m.manualCrash ? SettingsRepository().crash : 0}");
    }
    return data.toString();
  }

  void recordCrash() {
    _manualCrash = true;
  }

  void clear() {
    messages.clear();
    dataObserver.add(null);
  }

  void dispose() {
    _positionSubscription?.cancel();
    _accelerationSubscription?.cancel();
  }
}
