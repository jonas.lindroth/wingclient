import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:speed/account.dart';
import 'package:speed/data_collector.dart';
import 'package:speed/speed_gauge.dart';
import 'package:speed/utils.dart';
import 'chart.dart';
import 'model/data.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  SpeedGauge _speed;
  LinearChart _chart;
  DataCollector _dataCollector;
  StreamSubscription _subscription;
  PublishSubject<Data> _dataObserver = PublishSubject();

  LinearData _mapData(Data data) {
    return (data == null) ? null : LinearData("acc", data.sensor?.timestamp ?? data.position.timestamp, _mapAcc(data));
  }

  Map<String, double> _mapAcc(Data data) {
    if (data == null) return null;

    var map = Map<String, double>();

    map["x"] = data.sensor?.acceleration?.x ?? 0;
    map["y"] = data.sensor?.acceleration?.y ?? 0;
    map["z"] = data.sensor?.acceleration?.z ?? 0;
    map["a"] = data.analyzed;
    map["s"] = data.position != null ? Utils.getSpeedKmh(data.position) : 0;
    return map;
  }

  @override
  void initState() {
    super.initState();

    _dataCollector = DataCollector();
    var linearData = _dataCollector.messages.map((e) => _mapData(e)).toList();
    _chart = LinearChart.create(linearData);
    _subscription = _dataCollector.dataObserver.listen((value) {
      _chart.dataObservable.add(_mapData(value));
      _dataObserver.add(value);
    });
    _speed = SpeedGauge.withSubscriber(start:0, end:40, dataObservable:_dataObserver);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Column(
          children: <Widget>[
            GestureDetector(
                onDoubleTap: () {
                  _dataCollector.recordCrash();
                  Fluttertoast.showToast(
                      msg: 'Crash recorded at ${new DateFormat.yMMMd().add_Hms().format(DateTime.now())}.',
                      toastLength: Toast.LENGTH_LONG,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 10,
                      backgroundColor: Colors.blueGrey,
                      textColor: Colors.red,
                      fontSize: 16.0
                  );
                },
                child: new Padding(
                  padding: new EdgeInsets.all(40.0),
                  child: _speed,
                )
              ),
            Expanded(flex: 5, child: _chart),
          ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var messages = _dataCollector.getMessages();
          _dataCollector.uploadData(Account().user.displayName, messages).then((res) {
            Fluttertoast.showToast(
                msg: 'Uploaded ${_dataCollector.messages.length} items',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIosWeb: 3,
                backgroundColor: Colors.white,
                textColor: Colors.blueGrey,
                fontSize: 16.0
            );
            _dataCollector.clear();
          });
        },
        tooltip: 'Increment',
        child: Icon(Icons.upload_file),
      ),
    );
  }

  void dispose() {
    _subscription.cancel().then((value) {
      _dataObserver.close();
      _speed.dispose();

    });
    super.dispose();
  }
}