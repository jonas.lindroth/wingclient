import 'package:firebase_storage/firebase_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:speed/utils.dart';

class Location {
  final PublishSubject<Position> positionObservable = new PublishSubject();
  Position currentPosition;
  bool _started = false;

  Location._privateConstructor();

  static final Location _instance = Location._privateConstructor();

  factory Location() {
    _instance.start();
    return _instance;
  }

  start() {
    if (!_started)
      Geolocator.getPositionStream().listen(_onPosition);
  }

  void _onPosition(Position position) {
    currentPosition = position;
    positionObservable.add(position);
  }

  Future<Position> getPosition() {
    return Geolocator.getCurrentPosition();
  }
}