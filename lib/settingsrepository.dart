import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsRepository {
  SharedPreferences _prefs;
  SettingsRepository._privateConstructor();

  PublishSubject<SettingsRepository> settingsUpdated = new PublishSubject();

  static final SettingsRepository _instance = SettingsRepository._privateConstructor();

  factory SettingsRepository() {
    return _instance;
  }

  double get slidingWindowSize {
    return _prefs?.getDouble("slidingWindow") ?? 2000;
  }

  set slidingWindowSize(double size) {
    if (_prefs == null) return;

    _prefs.setDouble("slidingWindow", size);
    settingsUpdated.add(this);
  }

  bool get active {
    var a = _prefs?.getBool("active");
    return a ?? true;
  }

  set active(bool active) {
    if (_prefs == null) return;

    _prefs.setBool("active", active);
    settingsUpdated.add(this);
  }

  double get crash {
    var a = _prefs?.getDouble("crash");
    return a ?? -2;
  }

  set crash(double crash) {
    if (_prefs == null) return;

    _prefs.setDouble("crash", crash);
    settingsUpdated.add(this);
  }

  Future load() async {
    _prefs = await SharedPreferences.getInstance();
  }

  void dispose() {
    settingsUpdated.close();
  }
}