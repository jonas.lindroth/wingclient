import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:speed/accelerator.dart';
import 'package:speed/account.dart';
import 'package:speed/devices/bluetooth_device.dart';
import 'package:speed/devices/select_device_page.dart';
import 'package:speed/settingsrepository.dart';

class Settings extends StatefulWidget {
  @override
  _Settings createState() => _Settings();
}

class _Settings extends State<Settings> {
  String _deviceName;
  Icon _deviceIcon = Icon(Icons.bluetooth);
  StreamSubscription _statusSubscriber;
  double _windowSize;
  bool _active;
  final SettingsRepository _settingRepository = SettingsRepository();

  _Settings() {
    var accelerator = Accelerator();
    _windowSize = _settingRepository.slidingWindowSize;
    _active = _settingRepository.active;
    _deviceName = accelerator.deviceName;
    _deviceIcon = Icon(accelerator.isConnected ? Icons.bluetooth_connected : Icons.bluetooth_disabled);
    _statusSubscriber = accelerator.statusObserver.listen((value) {
      setState(() {
        _deviceName = value.device.name;
        _deviceIcon = Icon(value.availability == DeviceAvailability.yes ? Icons.bluetooth_connected : Icons.bluetooth_disabled);
      });
    });
  }
  Widget build(BuildContext context) {
    var deviceIcon = _deviceIcon;
    var deviceName = _deviceName;
    return Scaffold(
      body: SettingsList(
        // backgroundColor: Colors.orange,
        sections: [
        SettingsSection(
        title: 'Accelerometer',
        // titleTextStyle: TextStyle(fontSize: 30),
        tiles: [
          SettingsTile.switchTile(
            title: 'Active',
            leading: Icon(Icons.online_prediction),
            switchValue: _active,
            onToggle: (bool value) {
              setState(() {
                _active = value;
              });
              _settingRepository.active = value;
            },
          ),
          SettingsTile(
            title: 'Device',
            subtitle: deviceName,
            leading: deviceIcon,
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => SelectDevicePage()));
            },
          ),
          SettingsTile(
            title: 'Sliding Window',
            subtitle: '${_windowSize.round()} ms',
            leading: Icon(Icons.input),
            onTap: () {
              _showWindowSizeDialog();
            },
          ),
          SettingsTile(title: 'Crash Value', subtitle: SettingsRepository().crash.toString(), leading: Icon(Icons.access_alarms),onTap: () {
            _showWindowSizeDialog();
          }),
        ],
      ),
        SettingsSection(
        title: 'Account',
        tiles: [
          SettingsTile(title: 'Name', subtitle: Account().user.displayName, leading: Icon(Icons.phone)),
          SettingsTile(title: 'Email', subtitle: Account().user.email, leading: Icon(Icons.email)),
          SettingsTile(title: 'Sign out', leading: Icon(Icons.exit_to_app)),
        ],
        )
      ])
    );
  }

  void _showWindowSizeDialog() async {
    // <-- note the async keyword here

    // this will contain the result from Navigator.pop(context, result)
    final windowSize = await showDialog<double>(
      context: context,
      builder: (context) => WindowSizeDialog(windowSize: _windowSize),
    );

    SettingsRepository().slidingWindowSize = windowSize;
    // execution of this code continues when the dialog was closed (popped)

    // note that the result can also be null, so check it
    // (back button or pressed outside of the dialog)
    if (windowSize != null) {
      setState(() {
        _windowSize = windowSize;
      });
    }
  }

  void dispose() {
    _statusSubscriber.cancel();
    super.dispose();
  }
}

class WindowSizeDialog extends StatefulWidget {
  final double windowSize;

  WindowSizeDialog({Key key,  this.windowSize }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _WindowSizeDialog();
}

class _WindowSizeDialog extends State<WindowSizeDialog> {
  double _windowSize;
  @override
  void initState() {
    super.initState();
    _windowSize = widget.windowSize;
  }
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(title: Text('Sliding Window'),
      children: [
        Slider(
          value: _windowSize,
          min: 500,
          max: 5000,
          divisions: 10,
          label: _windowSize.round().toString(),
          onChanged: (double value) {
            setState(() {
              _windowSize = value;
            });
          },
        ),
        MaterialButton(child: new Text('Ok',
                              style: new TextStyle(fontSize: 16.0, color: Colors.green)), onPressed: () {
          Navigator.pop(context, _windowSize);
        })
      ],
    );
  }
}