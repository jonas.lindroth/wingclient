import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

enum DeviceAvailability {
  no,
  maybe,
  yes,
}

class Device {
  BluetoothDevice device;
  DeviceAvailability availability;
  int rssi;
  
  Device(this.device, this.availability, [this.rssi]);
}