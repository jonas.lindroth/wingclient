import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:rxdart/rxdart.dart';

import 'bluetooth_device.dart';
import 'bluetooth_devices.dart';

class Connection {
  BluetoothConnection connection;
  Device device;
  final PublishSubject<bool> connectionStatus = new PublishSubject();
  final PublishSubject<Uint8List> dataStream = new PublishSubject();
  StreamSubscription _dataSubscription;
  bool _active = false;
  bool isConnected = false;

  Connection(this.device);

  static Future<Connection> createWithName(String name, {bool connect = true}) async {
    var device = await BluetoothDevices.getDevice(name);
    var connection = Connection(device);
    connection.connect();
    return connection;
  }

  void _setDeviceStatus() {
    device.availability = isConnected ? DeviceAvailability.yes : DeviceAvailability.no;
    connectionStatus.add(isConnected);
  }

  Future connect() async {
    try {
      _active = true;
      connection = await BluetoothConnection.toAddress(device.device.address);
      _dataSubscription = connection.input.listen((event) {
        dataStream.add(event);
      });
      _dataSubscription.onDone(() {
        _reconnect();
      });
      isConnected = true;
    } on PlatformException catch(_) {
      _reconnect();
    } catch (_) {

    }
    _setDeviceStatus();
  }

  void disconnect() {
    _active = false;
    _dataSubscription.cancel();
    connection.close();
  }

  void _reconnect() {
    isConnected = false;
    if (!_active) return;

    Timer(Duration(milliseconds: 1000), connect);
  }
}