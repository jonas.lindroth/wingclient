import 'dart:async';

import 'package:flutter/material.dart';
import 'package:speed/devices/bluetooth_device.dart';
import 'package:speed/devices/bluetooth_devices.dart';
import './device_entry.dart';

class SelectDevicePage extends StatefulWidget {
  const SelectDevicePage();

  @override
  _SelectDevicePage createState() => new _SelectDevicePage();
}

class _SelectDevicePage extends State<SelectDevicePage> {
  BluetoothDevices _bluetoothDevices;
  StreamSubscription _subscriber;
  List<Device> _devices;
  _SelectDevicePage();

  @override
  void initState() {
    super.initState();
    _bluetoothDevices = BluetoothDevices();
    _subscriber = _bluetoothDevices.deviceObserver.listen((value) {
      _onDevice(value);
    });
    _bluetoothDevices.init();
  }

  void _onDevice(Device devices) {
    setState(() {

    });
  }

  @override
  void dispose() {
    super.dispose();
    _subscriber.cancel();
  }

  @override
  Widget build(BuildContext context) {
    var list = _bluetoothDevices.devices.values.map((d) => DeviceEntry(title:d.device.name, subTitle:d.device.address, connected:d.device.isConnected, bonded:d.device.isBonded, rssi:d.rssi)).toList();
    return Scaffold(
      body: ListView(children: list)
    );
  }
}
