import 'dart:async';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:speed/devices/bluetooth_device.dart';
import 'package:rxdart/rxdart.dart';

class BluetoothDevices {
  bool _isDiscovering = false;
  Map<String, Device> devices = new Map<String, Device>();
  StreamSubscription<BluetoothDiscoveryResult> _discoveryStreamSubscription;
  final PublishSubject<Device> deviceObserver = new PublishSubject();

  void init() {
    FlutterBluetoothSerial.instance.getBondedDevices().then((List<BluetoothDevice> bondedDevices) {
        for (var bDevice in bondedDevices) {
          devices[bDevice.address] = new Device(bDevice, DeviceAvailability.maybe);
          deviceObserver.add(devices[bDevice.address]);
        }
      });
  }

  void startDiscovery() {
      _isDiscovering = true;
      _discoveryStreamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
        if (!devices.containsKey(r.device.address))
          devices[r.device.address] = new Device(r.device, DeviceAvailability.maybe);
        else {
          devices[r.device.address].availability = DeviceAvailability.yes;
          devices[r.device.address].rssi = r.rssi;
        }
        deviceObserver.add(devices[r.device.address]);
      });

      _discoveryStreamSubscription.onDone(() {
        _isDiscovering = false;
    });
  }

  static bool _isDevice(BluetoothDevice device, String name) {
    return device?.name != null && device.name.startsWith(name);
  }

  static Future<Device> getDevice(String name) async {
    var btDevice = (await FlutterBluetoothSerial.instance.getBondedDevices()).firstWhere((device) => _isDevice(device, name));
    return new Device(btDevice, DeviceAvailability.maybe);
  }

  void dispose()
  {
    deviceObserver.close();
  }
}