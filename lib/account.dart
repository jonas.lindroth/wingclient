import 'package:speed/user.dart';

class Account {
  Account._privateConstructor();

  static final Account _instance = Account._privateConstructor();

  factory Account() {
    return _instance;
  }

  User user;
}