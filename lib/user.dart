import 'package:google_sign_in/google_sign_in.dart';

class User
{
  GoogleSignInAccount _googleSignInAccount;
  User(this._googleSignInAccount) ;
  String get displayName => _googleSignInAccount.displayName;
  String get email => _googleSignInAccount.email;
}