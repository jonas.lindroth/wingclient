import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:speed/signin.dart';
import 'package:speed/user.dart';

import 'account.dart';
import 'dashboard.dart';
import 'login.dart';

class Start extends StatefulWidget {
  @override
  _Start createState() => _Start();
}

class _Start extends State<Start> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: signIn(),
          builder: (_, AsyncSnapshot<GoogleSignInAccount> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.data != null) {
                Account().user = User(snapshot.data);
                return Dashboard(title: "Wing");
              }
              else
                return LoginPage();
            }
            else
              return CircularProgressIndicator();
          }
      ),
    );
  }
}

