import 'package:geolocator/geolocator.dart';
import 'package:speed/model/sensor.dart';

class Data {
  final Position position;
  final Sensor sensor;
  final double analyzed;
  final bool manualCrash;
  Data(this.position, this.sensor, this.analyzed, this.manualCrash);
}