import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:speed/model/acceleration.dart';

class Sensor {
  DateTime timestamp;
  final double temperature;
  final double height;
  final Acceleration acceleration;
  Sensor({this.acceleration, this.height, this.temperature}) {
    timestamp = DateTime.now();
  }
}